package ru.t1.volkova.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.response.AbstractUserResponse;
import ru.t1.volkova.tm.dto.model.UserDTO;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractUserResponse {

    public UserUnlockResponse(@Nullable final UserDTO user) {
        super(user);
    }

}
