package ru.t1.volkova.tm.exception.user;

public final class LoginExistsException extends AbstractUserException {

    public LoginExistsException() {
        super("Error! Login already exists...");
    }

}
