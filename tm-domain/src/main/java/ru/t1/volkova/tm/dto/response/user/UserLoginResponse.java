package ru.t1.volkova.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.response.AbstractResultResponse;
import ru.t1.volkova.tm.dto.response.AbstractUserResponse;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginResponse extends AbstractUserResponse {

    public UserLoginResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

    public UserLoginResponse(@Nullable final String token) {
        super(token);
    }

}
