package ru.t1.volkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.model.UserDTO;

public interface IUserDTORepository extends IAbstractDTORepository<UserDTO> {

    @Nullable
    UserDTO findOneByLogin(@NotNull String login);

    @Nullable
    UserDTO findOneByEmail(@NotNull String email);

}
