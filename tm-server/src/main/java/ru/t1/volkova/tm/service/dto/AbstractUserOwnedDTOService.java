package ru.t1.volkova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.repository.dto.IAbstractUserOwnedDTORepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.volkova.tm.dto.model.AbstractUserOwnedDTOModel;
import ru.t1.volkova.tm.exception.field.UserIdEmptyException;

public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedDTOModel, R extends IAbstractUserOwnedDTORepository<M>>
        extends AbstractDTOService<M, R> implements IUserOwnedDTOService<M> {

    public AbstractUserOwnedDTOService(
            @NotNull final R repository,
            @NotNull final IConnectionService connectionService
    ) {
        super(repository, connectionService);
    }

    @Override
    public void add(@NotNull final M entity) {
        if (entity.getUserId().isEmpty()) throw new UserIdEmptyException();
        super.add(entity);
    }

    @Override
    public void update(@NotNull final M entity) {
        if (entity.getUserId().isEmpty()) throw new UserIdEmptyException();
        super.update(entity);
    }


}
