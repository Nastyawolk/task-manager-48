package ru.t1.volkova.tm.api.repository.dto;

import ru.t1.volkova.tm.dto.model.SessionDTO;

public interface ISessionDTORepository extends IAbstractUserOwnedDTORepository<SessionDTO> {

}
