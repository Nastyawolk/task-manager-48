package ru.t1.volkova.tm.api.repository.dto;

import ru.t1.volkova.tm.dto.model.ProjectDTO;

public interface IProjectDTORepository extends IAbstractUserOwnedDTORepository<ProjectDTO> {

}
