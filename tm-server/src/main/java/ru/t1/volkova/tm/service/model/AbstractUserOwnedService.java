package ru.t1.volkova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.repository.dto.IAbstractUserOwnedDTORepository;
import ru.t1.volkova.tm.api.repository.model.IAbstractUserOwnedRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.volkova.tm.api.service.model.IUserOwnedService;
import ru.t1.volkova.tm.dto.model.AbstractUserOwnedDTOModel;
import ru.t1.volkova.tm.exception.field.UserIdEmptyException;
import ru.t1.volkova.tm.model.AbstractUserOwnedModel;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IAbstractUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(
            @NotNull final R repository,
            @NotNull final IConnectionService connectionService
    ) {
        super(repository, connectionService);
    }

    @Override
    public void add(@NotNull final M entity) {
        if (entity.getUser().getId().isEmpty()) throw new UserIdEmptyException();
        super.add(entity);
    }

    @Override
    public void update(@NotNull final M entity) {
        if (entity.getUser().getId().isEmpty()) throw new UserIdEmptyException();
        super.update(entity);
    }


}
