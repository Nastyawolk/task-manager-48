package ru.t1.volkova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.repository.dto.IAbstractDTORepository;
import ru.t1.volkova.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;

public abstract class AbstractDTORepository<E extends AbstractModelDTO> implements IAbstractDTORepository<E> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final E entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(@NotNull final E entity) {
        entityManager.merge(entity);
    }

}
