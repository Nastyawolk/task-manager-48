package ru.t1.volkova.tm.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.volkova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.volkova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.dto.model.ProjectDTO;
import ru.t1.volkova.tm.dto.model.TaskDTO;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.repository.dto.ProjectDTORepository;
import ru.t1.volkova.tm.repository.dto.TaskDTORepository;
import ru.t1.volkova.tm.repository.dto.UserDTORepository;
import ru.t1.volkova.tm.service.ConnectionService;
import ru.t1.volkova.tm.service.PropertyService;
import ru.t1.volkova.tm.tm.migration.AbstractSchemeTest;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TaskRepositoryTest extends AbstractSchemeTest {

    private static int NUMBER_OF_ENTRIES = 4;

    private static String USER_ID_1;

    @NotNull
    private final static List<TaskDTO> taskList = new ArrayList<>();;

    @NotNull
    private static final PropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @BeforeClass
    public static void initRepository() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull UserDTO user = new UserDTO();
            user.setFirstName("User");
            user.setLastName("User");
            user.setMiddleName("User");
            user.setEmail("user@mail.ru");
            user.setLogin("user");
            user.setPasswordHash("user");
            USER_ID_1 = user.getId();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        createTasks();
    }

    private static void createTasks()  {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            NUMBER_OF_ENTRIES = 4;
            entityManager.getTransaction().begin();
            @NotNull ProjectDTO project = new ProjectDTO();
            project.setUserId(USER_ID_1);
            projectRepository.add(project);
            for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
                @NotNull TaskDTO task = new TaskDTO();
                task.setName("task" + i);
                task.setDescription("Tdescription" + i);
                task.setUserId(USER_ID_1);
                task.setProjectId(project.getId());
                taskRepository.add(task);
                taskList.add(task);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testAddForUserId() {
        @NotNull final String name = "Test task";
        @NotNull final String description = "Test description";
        @NotNull TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(USER_ID_1);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            int expectedNumberOfEntries = taskRepository.getSize(USER_ID_1) + 1;
            entityManager.getTransaction().begin();
            taskRepository.add(task);
            Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(USER_ID_1));
            @Nullable final TaskDTO actualTask = taskRepository.findOneById(USER_ID_1, task.getId());
            Assert.assertNotNull(actualTask);
            Assert.assertEquals(name, actualTask.getName());
            Assert.assertEquals(description, actualTask.getDescription());
            Assert.assertEquals(USER_ID_1, actualTask.getUserId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @Nullable final List<TaskDTO> taskList = taskRepository.findAll(USER_ID_1);
            if (taskList != null)
            Assert.assertEquals(NUMBER_OF_ENTRIES, taskList.size());
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindAllForUserNegative() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @Nullable final List<TaskDTO> taskList = taskRepository.findAll((String) null);
            if (taskList != null)
                Assert.assertEquals(0, taskList.size());
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindOneByIdForUser() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            for (@NotNull final TaskDTO taskDTO : taskList) {
                Assert.assertEquals(taskDTO, taskRepository.findOneById(taskDTO.getUserId(), taskDTO.getId()));
            }
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            Assert.assertNull(taskRepository.findOneById(null, taskList.get(1).getId()));
            Assert.assertNull(taskRepository.findOneById(USER_ID_1, "NotExcitingId"));
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @Nullable final TaskDTO task = taskRepository.findOneByIndex(USER_ID_1, 1);
            @NotNull final TaskDTO expected = taskList.get(1);
            Assert.assertEquals(expected, task);
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindOneByIndexForUserNegative() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            Assert.assertNull(taskRepository.findOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES + 20));
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testRemoveOneByIdForUser() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            @Nullable final TaskDTO task = taskRepository.findOneById(USER_ID_1, taskList.get(0).getId());
            taskRepository.removeOneById(USER_ID_1, task.getId());
            entityManager.getTransaction().commit();
            assertNull(taskRepository.findOneById(USER_ID_1, taskList.get(0).getId()));
            taskList.remove(0);
            NUMBER_OF_ENTRIES = taskRepository.findAll(USER_ID_1).size();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testRemoveAllForUser() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clear(USER_ID_1);
            projectRepository.clear(USER_ID_1);
            entityManager.getTransaction().commit();
            assertEquals(0, taskRepository.getSize(USER_ID_1));
            taskList.clear();
            createTasks();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testRemoveAllForUserNegative() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clear("NotExcitingId");
            entityManager.getTransaction().commit();
            Assert.assertNotEquals(0, taskRepository.getSize(USER_ID_1));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testRemoveOneByIndexForUser() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            @Nullable final TaskDTO task = taskRepository.findOneByIndex(USER_ID_1, 1);
            taskRepository.removeOneByIndex(USER_ID_1, 1);
            entityManager.getTransaction().commit();
            Assert.assertNull(taskRepository.findOneById(USER_ID_1, task.getId()));
            taskList.remove(1);
            NUMBER_OF_ENTRIES =  taskRepository.findAll(USER_ID_1).size();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize(USER_ID_1));
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            @NotNull final ProjectDTO project = Objects.requireNonNull(projectRepository.findAll(USER_ID_1)).get(0);
            @Nullable final List<TaskDTO> tasks = taskRepository.findAllByProjectId(USER_ID_1, project.getId());
            Assert.assertNotNull(tasks);
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAllByProjectIdNegative() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            @NotNull final ProjectDTO project = projectRepository.findAll(USER_ID_1).get(0);
            @Nullable final List<TaskDTO> tasks = taskRepository.findAllByProjectId(null, project.getId());
            if (tasks == null) return;
            Assert.assertEquals(0, tasks.size());
        } finally {
            entityManager.close();
        }
    }

}
